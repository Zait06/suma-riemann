#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

#include "../Riemann/Riemann.h"

int main(int argc, char *argv[]){
    if(argc<4){
        cout<<"Forma de ejecutar: "<<argv[0]<<" a b n"<<endl;
        exit(0);
    }
    Riemann rei(atof(argv[1]), atof(argv[2]), atoi(argv[3]));
    rei.sumatoria();
    rei.saveFunction();
    rei.plotArea();
    return 0;
}