#include "Riemann.h"

Riemann::Riemann(float ra, float rb, int rn){
    a = ra;
    b = rb;
    n = rn;
    delta_x = (b - a) / ((float) rn);
    function = fopen("function.csv","w+");
    area = fopen("area.csv","w+");
    plt = gnuplot();
}

float Riemann::f(float x_i){
    return pow(x_i,2) + x_i + 5;
}

void Riemann::sumatoria(){
    sum = 0.0;
    float aux;
    for(int i=1; i<=n; i++){
        float new_x = a + (float(i) * delta_x);
        aux = f(new_x);
        fprintf(area,"%f,%f\n", new_x, aux);
        sum += (aux * delta_x);
    }
}

float Riemann::getSumatoria(){
    return sum;
}

void Riemann::saveFunction(){
    float res = 0.0;
    for(int i=(int) (a - 3); i<(int) (b + 3); i++){
        res = f((float) i);
        fprintf(function,"%d,%f\n", i, res);
    }
}

void Riemann::plotArea(){
    string msum = to_string(sum);
    string msg = "set title 'Area bajo la curva = ";
    msg += msum+" u²'";
    plt(msg.c_str());
    plt("set xlabel 'x'");
    plt("set ylabel 'f(x) = x²+x+5'");
    plt("set autoscale");
    plt("set datafile sep ','");
    plt("set grid lw 2");
    plt("plot 'function.csv' title 'Funcion' with lines ls 4 lt rgb 'red', 'area.csv' title 'Area' with boxes fs solid 0.25 linecolor rgb 'purple'");
}