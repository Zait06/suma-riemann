#ifndef RIEMANN_H_
#define RIEMANN_H_
#include <iostream>
#include <string.h>
#include <cmath>
using namespace std;

#include "../gnuplot/gnuplot.h"

class Riemann{
    private:
        float a, b;
        float delta_x;
        float sum;
        int n;
        FILE *function;
        FILE *area;
        gnuplot plt;
    public:
        Riemann(float,float,int);
        float f(float);
        void sumatoria();
        float getSumatoria();
        void saveFunction();
        void plotArea();
};

#endif